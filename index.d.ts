import {Schema} from "mongoose";

interface LocaleOptions {
    locales?: string[];
}

declare function ArrNoDupe(a: any[]): string[];

declare function localize(
    obj: any,
    locale: string,
    toJSON: boolean
): any;

declare function localizeOnly(
    obj: any,
    locale: string,
    localeDefault: string | undefined,
    toJSON: boolean
): any;

declare function addLocales(pathname: string, schema: Schema): void;

declare function recursiveIteration(schema: Schema): void;

declare function toJSONLocalized(
    this: any,
    obj: any,
    locale: string
): any;

declare function toObjectLocalized(
    this: any,
    obj: any,
    locale: string
): any;

declare function toJSONLocalizedOnly(
    this: any,
    obj: any,
    locale: string,
    localeDefault: string | undefined
): any;

declare function toObjectLocalizedOnly(
    this: any,
    obj: any,
    locale: string,
    localeDefault: string | undefined
): any;

declare function mongooseI18n(schema: Schema, options?: LocaleOptions): Schema;

export = mongooseI18n;